import vue from '@vitejs/plugin-vue'
import { defineConfig } from 'vite'
import { VitePWA } from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig({
  server: { port: 8090 },

  resolve: { alias: { '@': '/src' } },

  base: process.env.NODE_ENV === 'production' ? '/hogwarts-armory/' : '/',

  plugins: [
    vue(),
    VitePWA({
      // Assets from public dir
      includeAssets: ['favicon.svg', 'favicon.ico', 'robots.txt', 'apple-touch-icon.png', 'avatars/*.png'],
      // Global assets from src
      workbox: {
        globPatterns: ['**/*.{js,css,html,jpg,png}']
      },
      // PWA Manifest
      manifest: {
        name: 'Hogwarts Armory',
        short_name: 'Hogwarts Armory',
        description: 'Small armory app for Hogwarts Legacy',
        orientation: 'portrait',
        theme_color: '#1f2527',
        background_color: '#000000',
        icons: [
          {
            src: 'android-icon-192x192.png',
            sizes: '192x192',
            type: 'image/png',
            purpose: 'any maskable'
          }
        ]
      }
    })
  ]
})
