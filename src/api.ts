/**
 * Get/Set/Parse data saved in localStorage
 */
export type Type = 'hand' | 'face' | 'head' | 'neck' | 'robe' | 'outfit'
export type Rarity = 'common' | 'well-appointed' | 'superb' | 'extraordinary' | 'legendary'
export type Trait = {
  label: string
  description: string
}
export interface Gear {
  type: Type
  rarity: Rarity
  offense: number
  defense: number
  trait?: Trait
}

const LS_KEY = 'hogwarts-armory_gears'
const DEFAULT_DATA: Gear[] = [
  { type: 'hand', rarity: 'common', offense: 0, defense: 0, trait: undefined },
  { type: 'face', rarity: 'common', offense: 0, defense: 0, trait: undefined },
  { type: 'head', rarity: 'common', offense: 0, defense: 0, trait: undefined },
  { type: 'neck', rarity: 'common', offense: 0, defense: 0, trait: undefined },
  { type: 'robe', rarity: 'common', offense: 0, defense: 0, trait: undefined },
  { type: 'outfit', rarity: 'common', offense: 0, defense: 0, trait: undefined }
]

function getGears(): Gear[] {
  const savedData = localStorage.getItem(LS_KEY)

  if (savedData) return JSON.parse(savedData)
  else return DEFAULT_DATA
}

function saveGear(newGear: Gear) {
  const gears = getGears()
  const indexToUpdate = gears.findIndex((g) => g.type === newGear.type)
  gears[indexToUpdate] = newGear

  localStorage.setItem(LS_KEY, JSON.stringify(gears))
}

export { getGears, saveGear }
