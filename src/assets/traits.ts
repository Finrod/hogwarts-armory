import { Trait } from '@/api'

export const emptyTrait: Trait = { label: '', description: '' }

export const traits: Trait[] = [
  { label: 'Amortissement I', description: 'Réduit les dégâts infligés par les Trolls' },
  { label: 'Amortissement II', description: 'Réduit les dégâts infligés par les Trolls' },
  { label: 'Amortissement III', description: 'Réduit les dégâts infligés par les Trolls' },
  { label: 'Antivenin I', description: 'Réduit les dégâts infligés par les Araignées' },
  { label: 'Antivenin II', description: 'Réduit les dégâts infligés par les Araignées' },
  { label: 'Antivenin III', description: 'Réduit les dégâts infligés par les Araignées' },
  { label: 'Application I', description: 'Dégâts accrus avec tous les sorts de dégâts' },
  { label: 'Application II', description: 'Dégâts accrus avec tous les sorts de dégâts' },
  { label: 'Application III', description: 'Dégâts accrus avec tous les sorts de dégâts' },
  { label: 'Assourdissement I', description: 'Dégâts infligés avec une Mandragore accrus' },
  { label: 'Assourdissement II', description: 'Dégâts infligés avec une Mandragore accrus' },
  { label: 'Assourdissement III', description: 'Dégâts infligés avec une Mandragore accrus' },
  { label: 'Botanique I', description: 'Dégâts infligés par toutes les plantes grandement accrus' },
  { label: 'Botanique II', description: 'Dégâts infligés par toutes les plantes grandement accrus' },
  { label: 'Botanique III', description: 'Dégâts infligés par toutes les plantes grandement accrus' },
  { label: 'Bouclier Protego I', description: 'Réduit les dégâts infligés par les Mages noirs' },
  { label: 'Bouclier Protego II', description: 'Réduit les dégâts infligés par les Mages noirs' },
  { label: 'Bouclier Protego III', description: 'Réduit les dégâts infligés par les Mages noirs' },
  { label: 'Brûlure I', description: 'Dégâts infligés avec Incendio accrus' },
  { label: 'Brûlure II', description: 'Dégâts infligés avec Incendio accrus' },
  { label: 'Brûlure III', description: 'Dégâts infligés avec Incendio accrus' },
  {
    label: 'Conc. magie ancienne I',
    description: 'Vitesse de remplissage de la jauge de magie ancienne accrue'
  },
  {
    label: 'Conc. magie ancienne II',
    description: 'Vitesse de remplissage de la jauge de magie ancienne accrue'
  },
  {
    label: 'Conc. magie ancienne III',
    description: 'Vitesse de remplissage de la jauge de magie ancienne accrue'
  },
  { label: 'Contrôle I', description: 'Dégâts infligés avec un lancer de magie ancienne accrus' },
  { label: 'Contrôle II', description: 'Dégâts infligés avec un lancer de magie ancienne accrus' },
  { label: 'Contrôle III', description: 'Dégâts infligés avec un lancer de magie ancienne accrus' },
  { label: 'Crocs I', description: 'Dégâts des Choux mordeurs de Chine accrus' },
  { label: 'Crocs II', description: 'Dégâts des Choux mordeurs de Chine accrus' },
  { label: 'Crocs III', description: 'Dégâts des Choux mordeurs de Chine accrus' },
  { label: 'Cruauté I', description: 'Dégâts infligés avec Endoloris accrus' },
  { label: 'Cruauté II', description: 'Dégâts infligés avec Endoloris accrus' },
  { label: 'Cruauté III', description: 'Dégâts infligés avec Endoloris accrus' },
  { label: 'Désarmement I', description: 'Dégâts infligés avec Experlliarmus accrus' },
  { label: 'Désarmement II', description: 'Dégâts infligés avec Experlliarmus accrus' },
  { label: 'Désarmement III', description: 'Dégâts infligés avec Experlliarmus accrus' },
  { label: 'Destruction I', description: 'Dégâts infligés avec Confringo accrus' },
  { label: 'Destruction II', description: 'Dégâts infligés avec Confringo accrus' },
  { label: 'Destruction III', description: 'Dégâts infligés avec Confringo accrus' },
  {
    label: 'Embuscade I',
    description: 'Dégâts des sorts accrus en mode caché par le sortilège de Désillusion'
  },
  {
    label: 'Embuscade II',
    description: 'Dégâts des sorts accrus en mode caché par le sortilège de Désillusion'
  },
  {
    label: 'Embuscade III',
    description: 'Dégâts des sorts accrus en mode caché par le sortilège de Désillusion'
  },
  { label: 'Explosion I', description: 'Dégâts infligés avec Bombarda accrus' },
  { label: 'Explosion II', description: 'Dégâts infligés avec Bombarda accrus' },
  { label: 'Explosion III', description: 'Dégâts infligés avec Bombarda accrus' },
  { label: 'Impardonnables I', description: 'Dégâts infligés aux cibles maudites accrus' },
  { label: 'Impardonnables II', description: 'Dégâts infligés aux cibles maudites accrus' },
  { label: 'Impardonnables III', description: 'Dégâts infligés aux cibles maudites accrus' },
  { label: 'Lacération I', description: 'Dégâts accrus avec Diffindo' },
  { label: 'Lacération II', description: 'Dégâts accrus avec Diffindo' },
  { label: 'Lacération III', description: 'Dégâts accrus avec Diffindo' },
  { label: 'Liaison I', description: 'Dégâts infligés avec Petrificus Totalus accrus' },
  { label: 'Liaison II', description: 'Dégâts infligés avec Petrificus Totalus accrus' },
  { label: 'Liaison III', description: 'Dégâts infligés avec Petrificus Totalus accrus' },
  { label: 'Magie Ancienne I', description: 'Améliore les dégâts de la magie ancienne' },
  { label: 'Magie Ancienne II', description: 'Améliore les dégâts de la magie ancienne' },
  { label: 'Magie Ancienne III', description: 'Améliore les dégâts de la magie ancienne' },
  { label: 'Manipulation I', description: "La cible sous l'effet d'Impero inflige des dégâts accrus" },
  { label: 'Manipulation II', description: "La cible sous l'effet d'Impero inflige des dégâts accrus" },
  { label: 'Manipulation III', description: "La cible sous l'effet d'Impero inflige des dégâts accrus" },
  { label: 'Protection Amphibienne I', description: 'Réduit les dégâts infligés par les Fangieux' },
  { label: 'Protection Amphibienne II', description: 'Réduit les dégâts infligés par les Fangieux' },
  { label: 'Protection Amphibienne III', description: 'Réduit les dégâts infligés par les Fangieux' },
  { label: 'Protection Lupus I', description: 'Réduit les dégâts infligés par les Chiens et Loups' },
  { label: 'Protection Lupus II', description: 'Réduit les dégâts infligés par les Chiens et Loups' },
  { label: 'Protection Lupus III', description: 'Réduit les dégâts infligés par les Chiens et Loups' },
  { label: 'Protection Nécromantique I', description: 'Réduit les dégâts infligés par les Inferis' },
  { label: 'Protection Nécromantique II', description: 'Réduit les dégâts infligés par les Inferis' },
  { label: 'Protection Nécromantique III', description: 'Réduit les dégâts infligés par les Inferis' },
  { label: 'Résistance aux gobelins I', description: 'Réduit les dégâts infligés par les Gobelins' },
  { label: 'Résistance aux gobelins II', description: 'Réduit les dégâts infligés par les Gobelins' },
  { label: 'Résistance aux gobelins III', description: 'Réduit les dégâts infligés par les Gobelins' },
  { label: 'Venin I', description: 'Dégâts des Tentaculas vénéneuses accrus' },
  { label: 'Venin II', description: 'Dégâts des Tentaculas vénéneuses accrus' },
  { label: 'Venin III', description: 'Dégâts des Tentaculas vénéneuses accrus' }
]
